## Meme dealer

### Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Features](#setup)


### General info 

"Dark times have come. Memes are illegal. xXxDankScavengerxXx sells memes
as his way of earning a living."

Great xXxDankScavengerxXx doesn't have to worry anymore, coz there is a cool 
function that helps him to create the best set of memes, so he can sell them
for the highest price. 

/ This is my solution for the Python internship test. /

### Technologies

If you want to use this function all you need to have is Python 3.7 installed.
Then you can just run "meme_dealer.py".

"Meme_dealer.py" automatically imports function "calculate" from "main.py". 


### Features:
- sort your dank memes via size
- calculate meme value
- create best set of memes for the highest price
