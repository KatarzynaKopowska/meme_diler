#this is a cool function that calculates the best set of memes
#to sell them in best price ever!


def calculate(usb_size, memes):
    if usb_size == 1:
        remaining_size = 1024

    #sort memes by size
    sorted_memes = sorted(memes, key = lambda x: x[1], reverse=True)

    #extract memes data (name, size, price)
    meme_name = [x[0] for x in sorted_memes]
    meme_size = [x[1] for x in sorted_memes]
    meme_price = [x[2] for x in sorted_memes]

    #find the best set of memes - iteration
    final_price = 0
    memes_for_sale = []
    for meme in range(len(memes)):
        name = meme_name[meme]
        size = meme_size[meme]
        price = meme_price[meme]
    
        meme_package = []
        if size <= remaining_size:
            final_price += price
            remaining_size -= size
            meme_package.append(name)
            meme_package.append(price)
            memes_for_sale.append(meme_package)

    #create list of memes and prices
    memes_for_sale.reverse()
    final_memes = set([memes_for_sale[i][0] for i,meme in enumerate(memes_for_sale)])
    final_price = sum([(memes_for_sale[i][1]) for i, meme in enumerate(memes_for_sale)])

    #create meme bill
    good_stuff = final_price, final_memes
    
    print(good_stuff)
    return good_stuff

